<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Nev+aNp1UQ3QiZ5ubYca0own2uAF54mw7pBc77bX+9jMlP6dFqihoDV9JbJBAGmEJuqp3v/7CpLqRT6qlh9/4A==');
define('SECURE_AUTH_KEY',  'fcAyntBbwL7VSPgD5RxUuiJfq/VyktHFV5RgWCjQQE5cqM1qzALmyhI6Zp+LGe/Y2vAlzl9OybCpl8811HdhPg==');
define('LOGGED_IN_KEY',    'c0hbnQrDy6WpvEsY4QwG6b6w9o5+GQqIkj0icgQrj4lmltc0wS0DA7OqxjvjIssCg/hdfWCxyRVKIP4aX1Ozlw==');
define('NONCE_KEY',        'dh/Z6U7UOz7KkkVQABqN64QGFS556a6BYoJFjUB0q0TD2eV89fuqak489gtIYqkxAR6+T4XHkBwVHya3ky7IGA==');
define('AUTH_SALT',        'w4a39yZINx5FNSrgp4QiJERlqpCg1rcSNATjQfNFUZfoLHsnVkQh2ffYBNj+qh0HMFouFpodLOpd35a2yuQpvA==');
define('SECURE_AUTH_SALT', 'Ab/lG+fO5J4eFKfo4PwrQUFIttSMY7MY70h+SgW4VLxd76ZkPE/71vkr9rdm7FVIWnYJJczFVvu1Z/baJrx7RA==');
define('LOGGED_IN_SALT',   '7J5x3bNqelLkW4jb2nNTyFObvIxtZBuQErMtTCTz/i3ea+HBpJ47COqzOUjJ37OUGCiQqzVAu04mirdO0g/L9w==');
define('NONCE_SALT',       '2P6+szMkfLz/S8VhjwP/HdZEWXvOIfWLtRLrYhSJHJc3Vdg9IIdO5Fw7eIoMer1P/5XDMTjKIDfKOv9ma7JWAw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
